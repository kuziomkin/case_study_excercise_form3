from requests import get, post
from json import loads
from requests.auth import HTTPBasicAuth
import yaml

# Read the configuration file
with open('interfaces/config.yml', 'r') as f:
    config = yaml.safe_load(f)

# Retrieve the credentials from the configuration file
username = config['credentials']['username']
password = config['credentials']['password']

url = "https://api-m.sandbox.paypal.com"

#function to generate access token
def get_token():
    try:
        headers = {"Content-Type" : "application/x-www-form-urlencoded"}
        payload={"grant_type" : "client_credentials"}
        uri = url + "/v1/oauth2/token"
        res = post(uri, headers=headers, data=payload, auth = HTTPBasicAuth(username=username, password=password))
        return res.json()
    except Exception as e:
        return e

print(get_token()["access_token"])
    
